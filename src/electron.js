"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var electron_1 = require("electron");
var electron_is_dev_1 = __importDefault(require("electron-is-dev"));
var win;
var createWindow = function () {
    win = new electron_1.BrowserWindow({
        title: 'Alarm',
        width: 400,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
            webSecurity: false,
            enableRemoteModule: true
        }
    });
    if (electron_is_dev_1["default"]) {
        win.loadURL('http://localhost:3000');
    }
    else {
        win.loadURL("file://" + __dirname + "/index.html");
    }
    win.setMenu(null);
    // win.webContents.openDevTools();
};
electron_1.app.whenReady().then(function () {
    createWindow();
    electron_1.ipcMain.on('check-env', function (evt) { return (evt.returnValue = electron_is_dev_1["default"]); });
    electron_1.ipcMain.on('full-screen', function (evt) {
        win.setFullScreen(true);
        evt.returnValue = null;
    });
    electron_1.ipcMain.on('restore', function (evt) {
        win.setFullScreen(false);
        evt.returnValue = null;
    });
});
electron_1.app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        electron_1.app.quit();
    }
});
electron_1.app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (electron_1.BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});
