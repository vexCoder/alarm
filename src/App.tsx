import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Router } from './Router';
import Provider from './data/Provider';
import { isDev } from './utils/ipc';

interface AppProps {}

export const App: React.FC<AppProps> = ({}) => {
  return (
    <Provider>
      <div>
        <Router />
      </div>
    </Provider>
  );
};

export default isDev ? hot(App) : App;
