import { UploadOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React from 'react';

interface AlarmUploadProps {
  onChange: (dir: FileList | null) => void;
  file: string | null;
}

const AlarmUpload: React.FC<AlarmUploadProps> = ({ onChange }) => {
  const inputRef = React.useRef<any>(null);

  const handleClick = () => {
    if (inputRef.current) inputRef.current.click();
  };

  return (
    <>
      <input
        ref={inputRef}
        style={{
          position: 'fixed',
          userSelect: 'none',
          zIndex: -1,
          opacity: 0,
        }}
        type="file"
        onChange={(evt) => onChange && onChange(evt.target.files)}
      />
      <Button
        type="primary"
        icon={<UploadOutlined />}
        size="small"
        onClick={handleClick}
      >
        Select Sound
      </Button>
    </>
  );
};

export default AlarmUpload;
