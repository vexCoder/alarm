import { TimePicker } from 'antd';
import React from 'react';
import { Moment } from 'moment';
import styled from 'styled-components';

interface AlarmTimePickerProps {
  handleChange: (t: Moment | null) => void;
  value: Moment;
}

const AlarmTimePicker: React.FC<AlarmTimePickerProps> = ({
  handleChange,
  value,
}) => {
  return (
    <TimePickerWrapper
      style={{
        position: 'absolute',
        height: '100%',
        width: '100%',
        paddingLeft: '2em',
        cursor: 'pointer',
      }}
      value={value}
      format="hh:mm A"
      onChange={handleChange}
      size="large"
      bordered={false}
      suffixIcon={null}
      allowClear={false}
      inputReadOnly
    />
  );
};

export default AlarmTimePicker;

const TimePickerWrapper = styled(TimePicker)`
  input {
    height: 100%;
    width: 100%;
    cursor: pointer !important;
  }
`;
