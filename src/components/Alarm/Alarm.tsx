import { blue, red } from '@ant-design/colors';
import {
  BulbOutlined,
  DeleteFilled,
  ExclamationCircleFilled,
  PlusOutlined,
} from '@ant-design/icons';
import {
  Button,
  Checkbox,
  Divider,
  Modal,
  Slider,
  Space,
  Tooltip,
  Typography,
} from 'antd';
import Text from 'antd/lib/typography/Text';
import { ipcRenderer } from 'electron';
import fs from 'fs';
import { Howl } from 'howler';
import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import moment from 'moment';
import { nanoid } from 'nanoid';
import React from 'react';
import styled from 'styled-components';
import { useInterval } from '../../utils/hooks';
import AlarmClock from './AlarmClock';
import AlarmTimePicker from './AlarmTimePicker';
import AlarmUpload from './AlarmUpload';

const adapter = new FileSync('db.json');
const db = low(adapter);

interface AlarmProps {}

const Alarm: React.FC<AlarmProps> = ({}) => {
  const [file, setFile] = React.useState<string | null>(null);
  const [howl, setHowl] = React.useState<Howl | null>(null);
  const [ringing, setRinging] = React.useState<boolean>(false);
  const [fullScreen, setFullScreen] = React.useState<boolean>(false);
  const [volume, setVolume] = React.useState(0);
  const [alarms, setAlarms] = React.useState<
    { id: number; time: string; enabled: boolean; lastPlayed: string | null }[]
  >([]);

  React.useEffect(() => {
    db.defaults({ alarm: [], file: null, volume: 1 }).write();
    const filePath = db.get('file').value();
    const dbVolume = db.get('volume').value();
    setAlarms(db.get('alarm').value());
    setFile(filePath);
    setVolume(dbVolume * 100);
    Modal.destroyAll();
  }, []);

  React.useEffect(() => {
    if (file) {
      const sound = new Howl({
        src: [file],
        loop: true,
      });
      setHowl(sound);
    }
  }, [file]);

  useInterval(() => {
    const alarm = alarms.find((v) => {
      const diff = moment(v.time, 'HH:mm').diff(moment(), 'minute');
      const diffDay = v.lastPlayed
        ? moment().diff(moment(v.lastPlayed, 'MM/DD/YYYY HH:mm'), 'minute')
        : 1440;
      return diff === 0 && v.enabled && diffDay >= 24 * 60;
    });
    if (alarm && file && howl && !ringing) {
      const updateLastPlayed = (id: number) => {
        const arr = alarms.slice();
        const index = arr.findIndex((v) => v.id === id);
        arr[index] = {
          ...arr[index],
          lastPlayed: `${moment().format('MM/DD/YYYY')} ${arr[index].time}`,
        };
        setAlarms(arr);
        db.set('alarm', arr).write();
      };

      updateLastPlayed(alarm.id);
      howl.play();
      setRinging(true);
      Modal.warning({
        width: '60vh',
        icon: null,
        centered: true,
        content: (
          <div
            style={{
              display: 'grid',
              placeItems: 'center',
              width: '100%',
              height: '100%',
            }}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                gap: '2em',
              }}
            >
              <ExclamationCircleFilled
                style={{ fontSize: '5em', color: red[5] }}
              />
              <Button
                type="primary"
                onClick={() => {
                  if (howl) howl.stop();
                  console.log(howl);
                  setRinging(false);
                  Modal.destroyAll();
                }}
              >
                Ok! ill wake up!!
              </Button>
            </div>
          </div>
        ),
        okButtonProps: {
          hidden: true,
        },
      });
    }
  }, 1000);

  const toggleAlarm = (id: number, bool?: boolean) => {
    const arr = alarms.slice();
    const index = arr.findIndex((v) => v.id === id);
    arr[index] = {
      ...arr[index],
      enabled: bool != null ? bool : !arr[index].enabled,
    };
    setAlarms(arr);
    db.set('alarm', arr).write();
  };

  const editTime = (id: number, time: string) => {
    const arr = alarms.slice();
    const index = arr.findIndex((v) => v.id === id);
    arr[index] = {
      ...arr[index],
      time,
      lastPlayed: `${moment().subtract(1, 'day').format('MM/DD/YYYY')} ${time}`,
    };
    setAlarms(arr);
    db.set('alarm', arr).write();
  };

  const handleRemove = (id: number) => {
    const arr = alarms.slice().filter((v) => v.id !== id);
    setAlarms(arr);
    db.set('alarm', arr).write();
  };

  const handleAdd = () => {
    let newID;
    do {
      newID = nanoid();
    } while (alarms.find((v) => v.id === newID));
    const arr = alarms.slice();
    const newTimer = {
      id: newID,
      time: moment().format('HH:mm'),
      enabled: false,
      lastPlayed: null,
    };
    setAlarms([...arr, newTimer]);
    db.set('alarm', arr).write();
  };

  const handleSoundChange = (filelist: FileList | null) => {
    if (!filelist) return;
    const pathToSound = filelist[0].path;
    console.log();
    db.set('file', pathToSound).write();
    setFile(pathToSound);
  };

  const handleVolume = (value: number) => {
    setVolume(value);
  };

  const handleVolumeAfter = (value: number) => {
    const nVolume = value / 100;
    db.set('volume', nVolume).write();
    if (file) {
      const sound = new Howl({
        src: [file],
        loop: true,
        volume: nVolume,
      });
      setHowl(sound);
    }
  };

  const handleFullScreen = () => {
    setFullScreen(!fullScreen);
    ipcRenderer.sendSync(!fullScreen ? 'full-screen' : 'restore');
  };
  return (
    <>
      <div
        style={{
          position: 'fixed',
          width: '100vw',
          height: '100vh',
          background: 'black',
          display: 'grid',
          placeItems: 'center',
          transition: 'opacity 0.35s ease-in-out, z-index 1s ease-in-out',
          zIndex: fullScreen ? 1000 : 0,
          opacity: fullScreen ? 1 : 0,
        }}
      >
        <Space direction="vertical" align="center">
          <AlarmClock style={{ color: 'white' }} />
          <Button
            type="link"
            icon={<BulbOutlined style={{ color: 'white' }} />}
            onClick={handleFullScreen}
          />
        </Space>
      </div>
      <div
        style={{
          opacity: fullScreen ? 0 : 1,
        }}
      >
        <RootFlex>
          <div
            style={{
              width: '100%',
              height: 'auto',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
              padding: '1em',
            }}
          >
            <div
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'flex-end',
                marginBottom: '1em',
              }}
            >
              <AlarmClock style={{ color: blue[4], marginBottom: 0 }} />
              <Button
                type="link"
                icon={<BulbOutlined />}
                onClick={handleFullScreen}
              />
            </div>
            <Space>
              <AlarmUpload file={file} onChange={handleSoundChange} />
              <Text style={{ marginTop: '0.4em' }}>{file && file}</Text>
            </Space>
            <Slider
              min={1}
              max={100}
              value={volume}
              style={{ width: '100%', margin: 0, marginTop: '1em' }}
              onChange={handleVolume}
              onAfterChange={handleVolumeAfter}
            />
          </div>
          <Divider style={{ marginBottom: 0 }} />
          <div
            style={{ overflowY: 'auto', width: '100%', paddingBottom: '1em' }}
          >
            {alarms.map((v, i) => (
              <div
                key={i}
                style={{
                  position: 'relative',
                  height: '3em',
                  width: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                  padding: '0.2em 0.7em',
                  gap: '1em',
                  borderBottom: `1px solid #f0f0f0`,
                }}
              >
                <AlarmTimePicker
                  handleChange={(t) => t && editTime(v.id, t.format('HH:mm'))}
                  value={moment(v.time, 'HH:mm')}
                />
                <Space size="small">
                  <Checkbox
                    checked={v.enabled}
                    onChange={(e) => toggleAlarm(v.id, e.target.checked)}
                  />
                  <Tooltip title="Remove">
                    <Button
                      style={{
                        width: '35px',
                        height: '35px',
                        color: red[3],
                      }}
                      type="text"
                      shape="circle"
                      icon={<DeleteFilled />}
                      onClick={() => handleRemove(v.id)}
                    />
                  </Tooltip>
                </Space>
              </div>
            ))}
          </div>
          <div
            style={{
              position: 'fixed',
              bottom: 10,
              width: '100%',
              height: '10%',
              display: 'grid',
              placeItems: 'center',
            }}
          >
            <Tooltip title="Add Alarm">
              <Button
                style={{ width: '35px', height: '35px' }}
                type="primary"
                shape="circle"
                icon={<PlusOutlined />}
                onClick={() => handleAdd()}
              />
            </Tooltip>
          </div>
        </RootFlex>
      </div>
    </>
  );
};

export default Alarm;

const RootFlex = styled.div`
  display: flex;
  position: relative;
  width: 100vw;
  height: 100vh;
  flex-direction: column;
  align-items: center;
  .ant-divider-horizontal {
    margin-top: 0px;
    margin-bottom: 10px;
  }
`;
