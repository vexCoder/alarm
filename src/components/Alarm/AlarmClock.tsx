import { Typography } from 'antd';
import React from 'react';
import moment from 'moment';
import { useInterval } from '../../utils/hooks';

interface AlarmClockProps {
  style: React.CSSProperties;
}

const AlarmClock: React.FC<AlarmClockProps> = ({ style }) => {
  const [time, setTime] = React.useState(moment().format('hh:mm A'));

  useInterval(() => {
    setTime(moment().format('hh:mm A'));
  }, 1000);
  return (
    <Typography.Title level={1} style={style}>
      {time}
    </Typography.Title>
  );
};

export default AlarmClock;
