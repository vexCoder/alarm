import { app, BrowserWindow, ipcMain } from 'electron';
import isDev from 'electron-is-dev';

let win: BrowserWindow;

const createWindow = () => {
  win = new BrowserWindow({
    title: 'Alarm',
    width: 400,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false,
      enableRemoteModule: true,
    },
  });

  if (isDev) {
    win.loadURL('http://localhost:3000');
  } else {
    win.loadURL(`file://${__dirname}/index.html`);
  }

  win.setMenu(null);
  // win.webContents.openDevTools();
};

app.whenReady().then(() => {
  createWindow();
  ipcMain.on('check-env', (evt) => (evt.returnValue = isDev));
  ipcMain.on('full-screen', (evt) => {
    win.setFullScreen(true);
    evt.returnValue = null;
  });
  ipcMain.on('restore', (evt) => {
    win.setFullScreen(false);
    evt.returnValue = null;
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
