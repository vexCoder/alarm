import React from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';
import { Alarm } from './components/Alarm';
import { Routes } from './utils/config';

interface RouterProps {}

export const Router: React.FC<RouterProps> = ({}) => {
  return (
    <HashRouter>
      <Switch>
        <Route exact={true} path={Routes.Alarm} component={Alarm} />
      </Switch>
    </HashRouter>
  );
};
